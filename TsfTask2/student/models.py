from django.db import models
from course.models import Batch
# Create your models here.
class Student(models.Model):
    Name=models.CharField(max_length=40)
    UserName=models.CharField(max_length=40,primary_key=True)
    password=models.CharField(max_length=400)
    email=models.CharField(max_length=40)
    contact=models.CharField(max_length=10)

class EnrolledBatch(models.Model):
    StudentUserName=models.ForeignKey(Student,on_delete=models.CASCADE)
    BatchName=models.ForeignKey(Batch,on_delete=models.CASCADE)
