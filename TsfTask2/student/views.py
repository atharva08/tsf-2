from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import json
from student.models import Student,EnrolledBatch
from course.models import Batch
from student.serializers import studentSerializer,enrolledSerializer
# Create your views here.

@api_view(["POST"])
def add_student(request):
    try:
        Name=request.POST.get('Name')
        UserName=request.POST.get('UserName')
        password=request.POST.get('password')
        email=request.POST.get('email')
        contact=request.POST.get('contact')

        s=Student(Name=Name,UserName=UserName,password=password,email=email,contact=contact)
        s.save()
        return JsonResponse("Added Student "+s.UserName+" details",safe=False)
    except:
        return JsonResponse("Error",safe=False)

@api_view(["POST"])
def Enroll4Batch(request):
    StudentUserName=request.POST.get('StudentUserName')
    BatchName=request.POST.get('BatchName')
    print(BatchName)
    try:
        b=Batch.objects.get(BatchName=BatchName)

    except:
        return JsonResponse("No such Batch to enroll",safe=False)
    try:
        s=Student.objects.get(UserName=StudentUserName)
    except :
        return JsonResponse("No such Student in database",safe=False)
    exists=EnrolledBatch.objects.filter(StudentUserName=s,BatchName=b)
    if len(exists)==1:
        return JsonResponse("Student already enrolled for batch",safe=False)
    e=EnrolledBatch(StudentUserName=s,BatchName=b)
    e.save()
    return JsonResponse("Enrolled Student "+StudentUserName+" in batch "+BatchName +". ",safe=False)

@api_view(["GET"])
def all_students(request):
    if request.method == 'GET':
        #print('hiiiiiiii')
        s=Student.objects.all()
        serializer = studentSerializer(s, many=True)
        return Response(serializer.data)


@api_view(['GET', 'POST','DELETE'])
def student_ops(request,username):
    if request.method == 'DELETE':
        try:
            t=Student.objects.get(UserName=username)
        except:
            return JsonResponse("Error",safe=False)
        t.delete()
        return JsonResponse("Deleted teacher successfully",safe=False)

    elif request.method == 'GET':
        #print(username)
        #print('yoooo')
        try:
            s=Student.objects.filter(UserName=username)
            if len(s)==0:
                return JsonResponse("No such user in database",safe=False)
        except:

            return JsonResponse("Error",safe=False)
        e=EnrolledBatch.objects.filter(StudentUserName=username)
        print(e)
        serializer = studentSerializer(s, many=True)
        #print(enrolledSerializer(e,many=True))
        batch_serializer=enrolledSerializer(e, many=True)

        return Response([serializer.data,batch_serializer.data])

    elif request.method == 'POST':
        try:
            s = Student.objects.get(UserName=username)
            #print(t.Name)
        except :
            return JsonResponse("No such student in database",safe=False)
        s.Name=request.POST.get('Name')
        s.email=request.POST.get('email')
        s.contact=request.POST.get('contact')
        s.save()
        return JsonResponse("successfully updated student record",safe=False)
