from rest_framework import serializers
from course.models import Course,Batch
from student.models import Student,EnrolledBatch
from teacher.serializers import teacherSerializer
from course.serializers import batchSerializer,courseSerializer

class studentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['UserName','Name','email','contact']



class enrolledSerializer(serializers.ModelSerializer):
    StudentUserName=studentSerializer()
    BatchName=batchSerializer()
    class Meta:
        model = EnrolledBatch
        fields = '__all__'
