from django.urls import path
from . import views

urlpatterns=[
    path('add',views.add_student,name='add_student'),
    path('Enroll4Batch',views.Enroll4Batch,name='Enroll4Batch'),
    path('',views.all_students,name='all_students'),
    path('<str:username>',views.student_ops,name='student_ops'),

]
