# Generated by Django 2.1.1 on 2019-06-05 17:29

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('Name', models.CharField(max_length=40)),
                ('UserName', models.CharField(max_length=40, primary_key=True, serialize=False)),
                ('password', models.CharField(max_length=40)),
                ('email', models.CharField(max_length=40)),
                ('desc', models.CharField(max_length=400)),
                ('contact', models.CharField(max_length=40)),
            ],
        ),
    ]
