from django.urls import path
from . import views

urlpatterns=[
    path('add',views.add_teacher,name='add_teacher'),
    path('',views.all_course,name='all_course'),
    path('assign_batch',views.assign_batch,name='assign_batch'),
    path('batch',views.all_batch,name='all_batch'),
    #path('batch/<str:BatchName>',views.batch_ops,name='batch_ops'),
    path('<str:username>',views.teacher_ops,name='teacher_ops'),

]
