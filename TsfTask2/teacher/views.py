from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import json
from .serializers import teacherSerializer
from course.serializers import batchSerializer
from teacher.models import Teacher
from course.models import Course,Batch
# Create your views here.


@api_view(["POST"])
def add_teacher(request):
    try:
        Name=request.POST.get('Name')
        UserName=request.POST.get('UserName')
        password=request.POST.get('password')
        email=request.POST.get('email')
        desc=request.POST.get('desc')
        contact=request.POST.get('contact')

        t=Teacher(Name=Name,UserName=UserName,password=password,email=email,desc=desc,contact=contact)
        t.save()
        return JsonResponse("Added Teacherss details",safe=False)
    except:
        return JsonResponse("Teacher exists",safe=False)

@api_view(["GET"])
def all_course(request):
    if request.method == 'GET':
        #print('hiiiiiiii')
        t=Teacher.objects.all()
        serializer = teacherSerializer(t, many=True)
        return Response(serializer.data)


@api_view(['GET', 'POST','DELETE'])
def teacher_ops(request,username):
    if request.method == 'DELETE':
        try:
            t=Teacher.objects.get(UserName=username)
        except:
            return JsonResponse("Error",safe=False)
        t.delete()
        return JsonResponse("Deleted teacher successfully",safe=False)

    elif request.method == 'GET':
        print(username)
        #print('yoooo')
        try:
            t=Teacher.objects.filter(UserName=username)
            if len(t)==0:
                return JsonResponse("No such user in database",safe=False)
        except:
            return JsonResponse("Error",safe=False)
        b=Batch.objects.filter(TeacherUsername=username)
        batch_serializer=batchSerializer(b,many=True)
        serializer = teacherSerializer(t, many=True)
        return Response([serializer.data,batch_serializer.data])

    elif request.method == 'POST':
        try:
            t = Teacher.objects.get(UserName=username)
            print(t.Name)
        except Teacher.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        t.Name=request.POST.get('Name')
        t.email=request.POST.get('email')
        t.desc=request.POST.get('desc')
        t.contact=request.POST.get('contact')
        t.save()
        return JsonResponse("successfully updated record",safe=False)



@api_view(["POST"])
def assign_batch(request):
    CourseId=request.POST.get('CourseId')
    TeacherUsername=request.POST.get('TeacherUsername')
    #print(TeacherUsername)
    BatchName=request.POST.get('Batch')
    c=Course.objects.filter(CourseId=CourseId)
    #print('hiii')
    #print(c)
    if len(c)!=1:
        return JsonResponse("No such course in database",safe=False)
    t=Teacher.objects.filter(UserName=TeacherUsername)
    if len(t)!=1:
        return JsonResponse("No such teacher in database",safe=False)

    b=Batch.objects.filter(BatchName=BatchName)
    if len(b)!=1:
        return JsonResponse("Batch "+ BatchName +" does not exist",safe=False)


    t=Teaches(CourseId=c[0],TeacherUsername=t[0],Batch=b[0])
    t.save()
    return JsonResponse("Assigned batch "+t.Batch.BatchName +"  to  teacher "+t.TeacherUsername.Name+" .",safe=False)

@api_view(["GET"])
def all_batch(request):
    if request.method == 'GET':
        #print('hiiiiiiii')
        b=Batch.objects.all()
        serializer = batchSerializer(b, many=True)
        return Response(serializer.data)


'''
@api_view(['GET', 'POST','DELETE'])
def batch_ops(request,BatchName):
    if request.method == 'DELETE':
        try:
            b=Batch.objects.get(BatchName=BatchName)
        except:
            return JsonResponse("Batch does not exist",safe=False)
        b.delete()
        return JsonResponse("Deleted batch successfully",safe=False)

    elif request.method == 'GET':
        #print(username)
        #print('yoooo')
        try:
            b=Batch.objects.filter(BatchName=BatchName)
            if len(t)==0:
                return JsonResponse("No such batch in database",safe=False)
        except:
            return JsonResponse("Error",safe=False)
        b=Batch.objects.filter(BatchName=BatchName)
        serializer=batchSerializer(b,many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        try:
            b= Batch.objects.get(BatchName=BatchName)
            print(t.Name)
        except Batch.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        b.StartDate=request.POST.get('StartDate')
        b.EndDate=request.POST.get('EndDate')
        try:
            c=Course.objects.get(CourseId=request.POST.get('CourseId'))
        except:
            return JsonResponse("No such course in database",safe=False)

        t=Teacher.objects.filter(UserName=request.POST.get('TeacherUsername'))
        print(t)
        if len(t)==0:
            return JsonResponse("No such teacher in database",safe=False)
        b.CourseId=c
        b.TeacherUsername=t[0]
        b.save()
        return JsonResponse("successfully updated record",safe=False)
'''
