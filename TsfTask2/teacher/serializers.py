
from rest_framework import serializers

from teacher.models import Teacher

class teacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ['UserName','Name','email','desc','contact']
