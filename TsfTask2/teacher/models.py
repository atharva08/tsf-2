from django.db import models

# Create your models here.
class Teacher(models.Model):
    Name=models.CharField(max_length=40)
    UserName=models.CharField(max_length=40,primary_key=True)
    password=models.CharField(max_length=40)
    email=models.CharField(max_length=40)
    desc=models.CharField(max_length=400)
    contact=models.CharField(max_length=40)
