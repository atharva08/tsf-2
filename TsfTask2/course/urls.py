from django.urls import path
from . import views

urlpatterns=[
    path('add',views.add_course,name='add_course'),
    path('',views.all_courses,name='all_courses'),
    path('add_batch',views.add_batch,name='add_batch'),
    path('batch',views.all_batch,name='all_batch'),
    path('batch/<str:BatchName>',views.batch_ops,name='batch_ops'),
    path('<str:cid>',views.course_ops,name='course'),

]
