
from rest_framework import serializers

from course.models import Course,Batch
from teacher.serializers import teacherSerializer
class courseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class batchSerializer(serializers.ModelSerializer):
    CourseId=courseSerializer()
    TeacherUsername=teacherSerializer()
    class Meta:
        model = Batch
        fields = '__all__'
        



'''class batchSerializer(serializers.Serializer):
    BatchName=serializers.CharField(max_length=40)
    CourseId = courseSerializer( read_only=True)
    TeacherUsername=teacherSerializer(read_only=True)
    StartDate=serializers.DateField()
    EndDate=serializers.DateField()'''
