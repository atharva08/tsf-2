from django.db import models
from teacher.models import Teacher
# Create your models here.
class Course(models.Model):
    Name=models.CharField(max_length=40)
    CourseId=models.CharField(max_length=40,primary_key=True)
    Desc=models.CharField(max_length=400)
    Fees=models.CharField(max_length=40)

class Batch(models.Model):
    BatchName=models.CharField(max_length=40,primary_key=True)
    CourseId=models.ForeignKey(Course,on_delete=models.CASCADE)
    TeacherUsername=models.ForeignKey(Teacher,on_delete=models.CASCADE,default=0)
    StartDate=models.DateField()
    EndDate=models.DateField()
