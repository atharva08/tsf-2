from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import json
from .serializers import courseSerializer,batchSerializer
from teacher.models import Teacher
from course.models import Course,Batch
# Create your views here.
@api_view(["POST"])
def add_course(request):
        Name=request.POST.get('Name')
        CourseId=request.POST.get('CourseId')
        Desc=request.POST.get('Desc')
        Fees=request.POST.get('Fees')
        c=Course.objects.filter(CourseId=CourseId)
        if len(c)>0:
            return JsonResponse("Course "+ c[0].CourseId + " already exists",safe=False)
        c=Course(Name=Name,CourseId=CourseId,Desc=Desc,Fees=Fees)
        c.save()
        return JsonResponse("Added Course "+ c.Name + " details",safe=False)


@api_view(["GET"])
def all_courses(request):
    if request.method == 'GET':
        c=Course.objects.all()
        serializer = courseSerializer(c, many=True)
        return Response(serializer.data)
        #return Response([serializer.data,'yoo'])

@api_view(['GET', 'POST','DELETE'])
def course_ops(request,cid):
    if request.method == 'DELETE':
        try:
            c=Course.objects.get(CourseId=cid)
        except:
            return JsonResponse("No such Course",safe=False)
        c.delete()
        return JsonResponse("Deleted Course " +c.Name +" successfully",safe=False)

    elif request.method == 'GET':
        try:
            c=Course.objects.filter(CourseId=cid)
            if len(c)==0:
                return JsonResponse("No such Course in database",safe=False)
        except:
            return JsonResponse("Error",safe=False)
        b=Batch.objects.filter(CourseId=cid)
        batch_serializer=batchSerializer(b,many=True)
        serializer = courseSerializer(c, many=True)
        return Response([serializer.data,batch_serializer.data])

    elif request.method == 'POST':
        try:
            c = Course.objects.get(CourseId=cid)
            #print(c.Name)
        except Course.DoesNotExist:
            return JsonResponse("No such Course in database",safe=False)
        c.Desc=request.POST.get('Desc')
        c.Fees=request.POST.get('Fees')
        c.save()
        return JsonResponse("successfully updated course "+ c.Name +".",safe=False)

@api_view(["POST"])
def add_batch(request):
    BatchName=request.POST.get('BatchName')
    CourseId=request.POST.get('CourseId')
    TeacherUsername=request.POST.get('TeacherUsername')
    StartDate=request.POST.get('StartDate')
    EndDate=request.POST.get('EndDate')
    c=Course.objects.filter(CourseId=CourseId)
    if len(c)!=1:
        return JsonResponse("No such course in database",safe=False)
    b=Batch.objects.filter(BatchName=BatchName)
    if len(b)==1:
        return JsonResponse("batch already exists",safe=False)
    t=Teacher.objects.filter(UserName=TeacherUsername)
    if len(t)!=1:
        return JsonResponse("Teacher "+ TeacherUsername + " does not exists",safe=False)
    b=Batch(BatchName=BatchName,CourseId=c[0],StartDate=StartDate,EndDate=EndDate,TeacherUsername=t[0])
    b.save()
    return JsonResponse("Created new batch "+ b.BatchName + " successfully!",safe=False)

@api_view(["GET"])
def all_batch(request):
    if request.method == 'GET':
        b=Batch.objects.all()
        serializer = batchSerializer(b, many=True)
        return Response(serializer.data)

@api_view(['GET', 'POST','DELETE'])
def batch_ops(request,BatchName):
    if request.method == 'DELETE':
        try:
            b=Batch.objects.get(BatchName=BatchName)
        except:
            return JsonResponse("No such Batch",safe=False)
        b.delete()
        return JsonResponse("Deleted Batch successfully",safe=False)

    elif request.method == 'GET':
        try:
            b=Batch.objects.filter(BatchName=BatchName)
            if len(b)==0:
                return JsonResponse("No such Batch in database",safe=False)
        except:
            return JsonResponse("Error",safe=False)

        serializer = batchSerializer(b, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        try:
            b = Batch.objects.get(BatchName=BatchName)
        except Batch.DoesNotExist:
            return JsonResponse("No such Batch in database",safe=False)
        b.StartDate=request.POST.get('StartDate')
        b.EndDate=request.POST.get('EndDate')
        CourseId=request.POST.get('CourseId')
        c=Course.objects.filter(CourseId=CourseId)
        if len(c)!=1:
            return JsonResponse("No such course in database!! add Course first",safe=False)
        t=Teacher.objects.filter(UserName=request.POST.get('TeacherUsername'))
        if len(t)==0:
            return JsonResponse("No such teacher in database",safe=False)
        b.CourseId=c[0]
        b.TeacherUsername=t[0]
        b.save()
        return JsonResponse("successfully updated Batch "+ b.BatchName +".",safe=False)
